﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AZ11Pole;
using AZ11Pole.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AZTests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class RandomPolygonGeneratorTests
    {

        [TestMethod]
        public void GenerateRandomPolygonsTest()
        {
            const int verticesAmount = 1000;
            var polygons = GenerateRandomHullPolygons(verticesAmount);
            var tests = new List<bool>();
            foreach (var polygon in polygons)
            {
                //Assert.IsTrue(Algorithms.IsSimplePolygon(polygon));
                tests.Add(Algorithms.IsSimplePolygon(polygon));
            }
        }
        [TestMethod]
        public void GenerateRandomHullTestsToFile()
        {
            List<Polygon> polygons = new List<Polygon>();
            for (int i = 0; i < 3; i++)
            {
                polygons.AddRange(GenerateRandomHullPolygons((50 * i + 1) * 2000));
            }
            int count = 0;
            foreach (var polygon in polygons)
            {
                Assert.IsTrue(Algorithms.IsSimplePolygon(polygon), "Failed at: " + count);
                count++;
            }
            polygons = polygons.OrderBy(p => p.Points.Count).ToList();
            var minCount = polygons.First().Points.Count;
            var maxcount = polygons.Last().Points.Count;

            PolygonReader.WriteRandomPolygonsFile("randomHullPolygons.txt", polygons, false);
        }

        [TestMethod]
        public void GenerateRandomPolygonsOnCircleToFileTest()
        {
            List<Polygon> polygons = new List<Polygon>();
            int N = 200;
            polygons.AddRange(GenerateRandomCirclePolygons(N));
            var count = 0;
            foreach (var polygon in polygons)
            {
                Assert.IsTrue(Algorithms.IsSimplePolygon(polygon), "Failed at: " + count);
                PolygonReader.WriteRandomPolygonsFile("Polygon_" + N + "\\polygon_" + count + ".txt", polygon, false);

                count++;
            }
        }

        private List<Polygon> GenerateRandomCirclePolygons(int verticesAmount)
        {
            const int PolygonsAmount = 20;
            var list = new List<Polygon>();
            for (int i = 0; i < PolygonsAmount; i++)
            {
                list.Add(RandomPolygonGenerator.GeneraRanomCirclePolygon(verticesAmount));
            }
            return list;
        }

        private List<Polygon> GenerateRandomHullPolygons(int verticesAmount)
        {
            const int PolygonsAmount = 20;

            var polygons = new List<Polygon>();
            for (int i = 0; i < PolygonsAmount; i++)
            {
                polygons.Add(RandomPolygonGenerator.GenerateRandomHullPolygon(verticesAmount));
            }
            return polygons.OrderBy(p => p.Points.Count).ToList();
        }
    }
}
