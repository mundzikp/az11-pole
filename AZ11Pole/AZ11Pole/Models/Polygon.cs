﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AZ11Pole.Models
{
    public class Polygon
    {
        public Polygon(List<Point> listOfPoints, Point pointToCheck)
        {
            this.Points = listOfPoints;
            this.PointToCheck = pointToCheck;
        }

        public List<Point> Points { get; set; }

        //Punkt którego przynależność do wielokąta będzie sprawdzana
        public Point PointToCheck { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var point in this.Points)
            {
                sb.Append(String.Format("({0}, {1})", point.X.ToString(CultureInfo.InvariantCulture), point.Y.ToString(CultureInfo.InvariantCulture)) + " ");
            }
            sb.Append(string.Format("|A: ({0}, {1})", PointToCheck.X.ToString(CultureInfo.InvariantCulture), PointToCheck.Y.ToString(CultureInfo.InvariantCulture)));
            return sb.ToString();
        }

        //public void SortPolygonPointsCounterClockWise()
        //{
        //    Point[] points = this.Points.ToArray();
        //    Comparison<Point> comparison = new Comparison<Point>(PolygonReader.ComparePoints);
        //    Array.Sort(points, comparison);
        //    this.Points = points.ToList();
        //}
    }
}