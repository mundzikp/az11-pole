﻿using System;
using System.Collections.Generic;
using System.Linq;
using AZ11Pole.Models;

namespace AZ11Pole
{
    public static class RandomPolygonGenerator
    {
        static readonly double TOLERANCE = Double.Epsilon;

        private static readonly int _circleRound = 8;
        private static readonly int _hullRound = 3;
        private static readonly int _circleMultiplier = 10;
        private static readonly int _hullMultiplier = 20;


        static readonly Random rand = new Random();
        public static Polygon GenerateRandomHullPolygon(int maxVerticesAmount)
        {
            if (maxVerticesAmount < 3)
                return null;

            List<Point> points = new List<Point>();
            for (int i = 0; i < maxVerticesAmount; i++)
            {
                points.Add(RandomPoint(_hullMultiplier, _hullRound));
            }
            points.Distinct();
            var newPoints = convex_hull(points.ToArray()).ToList();
            return new Polygon(newPoints, RandomPoint(_hullMultiplier + 5, _hullRound, 2.5));
        }

        public static Polygon GeneraRanomCirclePolygon(int verticesAmount)
        {
            if (verticesAmount < 3)
            {
                return null;
            }

            return new Polygon(GetRandomPointsOnCircle(verticesAmount), RandomPoint(2*_circleMultiplier, _circleRound, _circleMultiplier/2));
        }

        private static Point[] convex_hull(Point[] P)
        {
            var hList = new List<Point>();
            if (P.Length > 1)
            {
                int n = P.Length, k = 0;
                Point[] H = new Point[2 * n];

                P = P.OrderBy(p => p.X).ThenBy(p => p.Y).ToArray();

                // Build lower hull
                for (int i = 0; i < n; ++i)
                {
                    while (k >= 2 && Cross(H[k - 2], H[k - 1], P[i]) <= 0)
                        k--;
                    H[k++] = P[i];
                }

                // Build upper hull
                for (int i = n - 2, t = k + 1; i >= 0; i--)
                {
                    while (k >= t && Cross(H[k - 2], H[k - 1], P[i]) <= 0)
                        k--;
                    H[k++] = P[i];
                }
                if (k > 1)
                {
                    Point[] newH = new Point[k - 1];
                    Array.Copy(H, newH, k - 1);
                    H = newH;
                }
                return H;
            }
            if (P.Length <= 1)
            {
                return P;
            }
            return null;
        }

        private static List<Point> GetRandomPointsOnCircle(int verticesNumber)
        {
            var angles = GetRandomAnglesOnTheCircle_One(verticesNumber);
            var radius = RandomDouble(_circleMultiplier, _circleRound);
            var origin = RandomPoint(_circleMultiplier, _circleRound);

            var pointsList = new List<Point>();
            foreach (var angle in angles)
            {
                pointsList.Add(GetPointOnTheCircle(angle, radius, origin));
            }

            return pointsList;
        }

        private static Point RandomPoint(int multiplier, int round, double shiftBack = 0)
        {
            return new Point(RandomDouble(multiplier, round) - shiftBack,
                RandomDouble(multiplier, round) - shiftBack);
        }

        private static double RandomDouble(int multiplier, int round)
        {
            return Math.Round(rand.NextDouble() * multiplier, round);
        }

        private static List<double> GetRandomAnglesOnTheCircle_One(int N)
        {
            var randomAngles = new List<double>();
            var ratio = 2 * Math.PI / N;
            var counter = 0;
            for (double alfa = 0; alfa < 2 * Math.PI && counter<N; alfa += ratio, counter++)
            {
                randomAngles.Add(alfa + RandomDouble(3, _circleRound) % ratio);
            }

            return randomAngles;
        }

        private static Point GetPointOnTheCircle(double angle, double radius, Point origin)
        {
            return new Point(origin.X + radius * Math.Cos(angle),
                origin.Y + radius * Math.Sin(angle));
        }

        private static double Cross(Point O, Point A, Point B)
        {
            return (A.X - O.X) * (B.Y - O.Y) - (A.Y - O.Y) * (B.X - O.X);
        }
    }
}
