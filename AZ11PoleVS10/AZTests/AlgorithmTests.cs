﻿using System.Collections.Generic;
using AZ11Pole;
using AZ11Pole.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AZTests
{
    [TestClass]
    public class AlgorithmTests
    {
        [TestMethod]
        public void DoIntersectTest()
        {
            Point p1 = new Point(10, 0);
            Point q1 = new Point(0, 10);
            Point p2 = new Point(0, 0);
            Point q2 = new Point(10, 10);

            Assert.IsTrue(Algorithms.DoIntersect(p1, q1, p2, q2));
        }

        [TestMethod]
        public void DoNotIntersectTest1()
        {
            Point p1 = new Point(1, 1);
            Point q1 = new Point(10, 1);
            Point p2 = new Point(1, 2);
            Point q2 = new Point(10, 2);

            Assert.IsFalse(Algorithms.DoIntersect(p1, q1, p2, q2));
        }

        [TestMethod]
        public void DoNotIntersectTest2()
        {
            Point p1 = new Point(5, -5);
            Point q1 = new Point(0, 0);
            Point p2 = new Point(1, 1);
            Point q2 = new Point(10, 10);

            Assert.IsFalse(Algorithms.DoIntersect(p1, q1, p2, q2));
        }

        [TestMethod]
        public void PolygonIsNotSimpleTest()
        {
            var polygon = new Polygon(new List<Point>
            {
                new Point(0,0),
                new Point(0,10),
                new Point(10, 10),
                new Point(10, -5),
                new Point(-5, 10)
            }, new Point(5, 5));
            Assert.IsFalse(Algorithms.IsSimplePolygon(polygon));
            Assert.AreEqual(ProblemStatus.NotSimple.Value, Algorithms.IsPointInsidePolygon(polygon).Value);
        }

        [TestMethod]
        public void PolygonIsSimpleTest()
        {
            var polygon = new Polygon(new List<Point>
            {
                new Point(0,0),
                new Point(0,10),
                new Point(10, 10),
                new Point(10, -5)
            }, new Point(5, 5));
            Assert.IsTrue(Algorithms.IsSimplePolygon(polygon));
            Assert.AreEqual(ProblemStatus.PointIsInside.Value, Algorithms.IsPointInsidePolygon(polygon).Value);
        }

        [TestMethod]
        public void PointIsInsideTest1()
        {
            var polygon = PolygonInstance1(new Point(5, 5));

            Assert.AreEqual(ProblemStatus.PointIsInside.Value, Algorithms.IsPointInsidePolygon(polygon).Value);
        }

        [TestMethod]
        public void PointIsNotInsideTest1()
        {
            var polygon = PolygonInstance1(new Point(20, 20));

            Assert.AreEqual(ProblemStatus.PointIsOutside.Value, Algorithms.IsPointInsidePolygon(polygon).Value);
        }

        [TestMethod]
        public void PointIsNotInsideTest2()
        {
            var polygon = PolygonInstance1(new Point(-1, 10));

            Assert.AreEqual(ProblemStatus.PointIsOutside.Value, Algorithms.IsPointInsidePolygon(polygon).Value);
        }

        [TestMethod]
        public void PointIsInsideTest2()
        {
            var polygon = PolygonInstance2(new Point(3, 3));

            Assert.AreEqual(ProblemStatus.PointIsInside.Value, Algorithms.IsPointInsidePolygon(polygon).Value);
        }

        [TestMethod]
        public void PointIsInsideTest3()
        {
            var polygon = PolygonInstance2(new Point(5, 1));

            Assert.AreEqual(ProblemStatus.PointIsInside.Value, Algorithms.IsPointInsidePolygon(polygon).Value);
        }

        public void PointIsNotInsideTest3()
        {
            var polygon = PolygonInstance2(new Point(8, 1));

            Assert.AreEqual(ProblemStatus.PointIsOutside.Value, Algorithms.IsPointInsidePolygon(polygon).Value);
        }

        public Polygon PolygonInstance1(Point pointToCheck)
        {
            return new Polygon(new List<Point> {
                new Point(0, 0),
                new Point(10, 0),
                new Point(10, 10),
                new Point(0, 10)
            }, pointToCheck);
        }

        public Polygon PolygonInstance2(Point pointToCheck)
        {
            return new Polygon(new List<Point> {
                new Point(0, 0),
                new Point(5,5),
                new Point(5, 0)
            }, pointToCheck);
        }
    }
}
