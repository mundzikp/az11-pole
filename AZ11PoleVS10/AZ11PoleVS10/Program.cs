﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using AZ11Pole.Models;

namespace AZ11Pole
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            StringBuilder stringBuilder = new StringBuilder();
            ReadFile(ref stringBuilder);
            while (Console.ReadKey().Key.Equals(ConsoleKey.Y))
            {
                Console.WriteLine();
                Console.WriteLine();
                ReadFile(ref stringBuilder);
            }
            PolygonReader.WritePolygons("results" + ".txt", stringBuilder.ToString());
        }

        private static void ReadFile(ref StringBuilder stringBuilder)
        {
            try
            {
                stringBuilder.AppendLine(ReadProblem());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Do you want to read another file? (Y/N)");
        }

        private static string ReadProblem()
        {
            //Lista wielokątów, których wierzchołki posorotwane są przeciwnie do ruchu wskazówek zegara
            List<Polygon> polygons = PolygonReader.ReadPolygons();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var polygon in polygons)
            {
                string msg1 = string.Format("Polygon: {0}", polygon.ToString());
                stringBuilder.AppendLine(msg1);
                Console.WriteLine(msg1);

                ProblemStatus isPointInsideResult = Algorithms.IsPointInsidePolygon(polygon);
                stringBuilder.AppendLine(isPointInsideResult.Value);
                Console.WriteLine(isPointInsideResult.Value);

                if (isPointInsideResult.Value != ProblemStatus.NotSimple.Value &&
                    isPointInsideResult.Value != ProblemStatus.WronglyDefined.Value)
                {
                    double polygonArea = Algorithms.PolygonArea(polygon);
                    string msg2 = string.Format("Area of the polygon: [{0}]", polygonArea.ToString(CultureInfo.InvariantCulture));
                    stringBuilder.AppendLine(msg2);
                    Console.WriteLine(msg2);
                }
                stringBuilder.AppendLine();
                Console.WriteLine();

            }
            return stringBuilder.ToString();
        }
    }
}