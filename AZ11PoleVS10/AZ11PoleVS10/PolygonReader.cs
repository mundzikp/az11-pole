﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AZ11Pole.Models;

namespace AZ11Pole
{
    public static class PolygonReader
    {
        //Punkt wykorzystywany w sortowaniu wczytanych wierzchołków wielokąta 
        private static Point firstPoint;

        private const double TOLERANCE = 1e-10;

        public static List<Polygon> ReadPolygons()
        {
            List<Polygon> listOfPolygons = new List<Polygon>();

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            ofd.InitialDirectory = Path.Combine(Environment.CurrentDirectory);
            ofd.Filter = "Text|*.txt|All|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in ofd.FileNames)
                {
                    StreamReader sr = new StreamReader(file);
                    List<string> lines = new List<string>();
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                    sr.Close();
                    listOfPolygons.Add(ReadPolygonFromLine(lines.ToArray()));
                }
            }
            return listOfPolygons;
        }

        private static Polygon ReadPolygonFromLine(string[] lines)
        {
            if (lines.Length != 2)
                throw new Exception("Wrong polygon file format");
            string polygonPointsLine = lines[0];
            string pointToCheckLine = lines[1];

            //Wczytywanie punktów wielokąta
            List<Point> points = new List<Point>();
            var pointsString = polygonPointsLine.Split('|');
            foreach (var pointString in pointsString)
            {
                string pointCoordsString = pointString.Split('(', ')')[1];
                string[] pointCoords = pointCoordsString.Split(',');
                double x0, y0;
                if (!Double.TryParse(pointCoords[0], NumberStyles.AllowThousands | NumberStyles.Float, CultureInfo.InvariantCulture, out x0))
                {
                    throw new Exception("Wrong polygon points definition in a file.");
                }
                if (!Double.TryParse(pointCoords[1], NumberStyles.AllowThousands | NumberStyles.Float, CultureInfo.InvariantCulture, out y0))
                {
                    throw new Exception("Wrong polygon points definition in a file.");
                }
                points.Add(new Point(x0, y0));
            }

            //Wczytywanie punktu, którego przynależność do wielokąta będzie sprawdzana 
            string pointToCheckCoordsString = pointToCheckLine.Split('(', ')')[1];
            string[] pointToCheckCoords = pointToCheckCoordsString.Split(',');
            double x1, y1;
            if (!Double.TryParse(pointToCheckCoords[0], NumberStyles.AllowThousands | NumberStyles.Float, CultureInfo.InvariantCulture, out x1))
            {
                throw new Exception("Wrong point to check  definition in a file.");
            }
            if (!Double.TryParse(pointToCheckCoords[1], NumberStyles.AllowThousands | NumberStyles.Float, CultureInfo.InvariantCulture, out y1))
            {
                throw new Exception("Wrong point to check definition in a file.");
            }

            var polygon = new Polygon(points, new Point(x1, y1));
            //firstPoint = GetHighestPoint(polygon.Points);
            //polygon.SortPolygonPointsCounterClockWise();
            return polygon;
        }

        public static void ClearResultsDirectory()
        {
            string resultsDir = Path.Combine(Environment.CurrentDirectory);

            System.IO.DirectoryInfo dir = new DirectoryInfo(resultsDir);
            foreach (FileInfo file in dir.GetFiles())
            {
                file.Delete();
            }
        }

        public static void WritePolygons(string fileName, string programInfo)
        {
            string resultsDir = Path.Combine(Environment.CurrentDirectory);

            using (StreamWriter sw = File.CreateText(Path.Combine(resultsDir, fileName)))
            {
                sw.Write(programInfo);
                sw.Close();
            }
        }

        public static void WriteRandomPolygonsFile(string fileName, List<Polygon> polygons, bool append = true)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var polygon in polygons)
            {
                string line = null;
                foreach (var point in polygon.Points)
                {
                    line += "(" + point.X + "," + point.Y + ")|";
                }
                line = line.Remove(line.Length - 1);
                sb.AppendLine(line);
                sb.AppendLine("(" + polygon.PointToCheck.X + "," + polygon.PointToCheck.Y + ")");
                sb.AppendLine();
            }

            string resultsDir = Path.Combine(Environment.CurrentDirectory);
            var filemode = append ? FileMode.Append : FileMode.Create;
            using (FileStream fs = new FileStream(Path.Combine(resultsDir, fileName), filemode, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.Write(sb.ToString());
                }
            }

        }

        public static void WriteRandomPolygonsFile(string fileName, Polygon polygon, bool append = true)
        {
            StringBuilder sb = new StringBuilder();
            {
                string line = null;
                foreach (var point in polygon.Points)
                {
                    line += "(" + point.X + "," + point.Y + ")|";
                }
                line = line.Remove(line.Length - 1);
                sb.AppendLine(line);
                sb.AppendLine("(" + polygon.PointToCheck.X + "," + polygon.PointToCheck.Y + ")");

                string resultsDir = Path.Combine(Environment.CurrentDirectory);
                var filemode = append ? FileMode.Append : FileMode.Create;
                using (FileStream fs = new FileStream(Path.Combine(resultsDir, fileName), filemode, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.Write(sb.ToString());
                    }
                }
            }
        }

            //private static Point GetHighestPoint(IList<Point> points)
            //{
            //    Point maxPoint = new Point(0, Double.MinValue);
            //    foreach (var point in points)
            //    {
            //        if (point.Y > maxPoint.Y)
            //            maxPoint = point;
            //        else if (Math.Abs(point.Y - maxPoint.Y) < TOLERANCE)
            //            if (point.X > maxPoint.X)
            //                maxPoint = point;
            //    }
            //    return maxPoint;
            //}

            ////Sortowanie punktów: Counterclockwise
            //private static bool LessRelation(Point a, Point b)
            //{
            //    Point center = new Point(firstPoint.X, firstPoint.Y);
            //    if (Math.Abs(a.X - center.X) < TOLERANCE && Math.Abs(b.X - center.X) < TOLERANCE)
            //    {
            //        if (Math.Abs(a.Y - center.Y) < TOLERANCE || Math.Abs(b.Y - center.Y) < TOLERANCE)
            //            return a.Y > b.Y;
            //        return b.Y > a.Y;
            //    }

            //    double det = (a.X - center.X) * (b.Y - center.Y) - (b.X - center.X) * (a.Y - center.Y);
            //    if (det < 0)
            //        return true;
            //    if (det > 0)
            //        return false;

            //    double d1 = (a.X - center.X) * (a.X - center.X) + (a.Y - center.Y) * (a.Y - center.Y);
            //    double d2 = (b.X - center.X) * (b.X - center.X) + (b.Y - center.Y) * (b.Y - center.Y);
            //    return d1 > d2;
            //}

            //public static int ComparePoints(Point a, Point b)
            //{
            //    return LessRelation(a, b) ? 1 : -1;
            //}
        }
    }