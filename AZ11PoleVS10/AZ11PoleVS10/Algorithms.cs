﻿using System;
using System.Diagnostics;
using System.Linq;
using AZ11Pole.Models;

namespace AZ11Pole
{
    public static class Algorithms
    {
        /// <summary>
        /// Algorytm obliczający pole zadanego wielokąta
        /// </summary>
        /// <param name="polygon">Procesowany wielokąt</param>
        /// <returns>Pole wielokąta</returns>
        public static double PolygonArea(Polygon polygon)
        {
            Point[] polygonPoints = polygon.Points.ToArray();
            double sum = 0;
            for (int i = 1; i <= polygonPoints.Length; i++)
            {
                int iIndex = i - 1;
                int iPlusOneIndex = i;
                if (iPlusOneIndex == polygonPoints.Length)
                {
                    iPlusOneIndex = 0;
                }
                sum += (polygonPoints[iIndex].X * polygonPoints[iPlusOneIndex].Y -
                        polygonPoints[iPlusOneIndex].X * polygonPoints[iIndex].Y);
            }
            var result = 0.5 * Math.Abs(sum);
            return result;
        }

        public static ProblemStatus IsPointInsidePolygon(Polygon polygon)
        {
            var pointsCount = polygon.Points.Count;
            if (pointsCount < 3)
            {
                return ProblemStatus.WronglyDefined;
            }
            if (!IsSimplePolygon(polygon))
            {
                return ProblemStatus.NotSimple;
            }
            
            var r = GetPointOutsidePolygon(polygon);
            int count = 0;

            for (var i = 0; i < pointsCount; i++)
            {
                var next = (i + 1) % pointsCount;
                if (DoIntersect(
                    polygon.Points[i], polygon.Points[next],
                    polygon.PointToCheck, r))
                {
                    if (SegmentsOrientation(polygon.Points[i], polygon.PointToCheck, polygon.Points[next]) == 0)
                    {
                        var result = IsOnSegment(polygon.Points[i], polygon.PointToCheck, polygon.Points[next])
                            ? ProblemStatus.PointIsInside
                            : ProblemStatus.PointIsOutside;
                        return result;
                    }
                    count++;
                }
            }

            var result2 = (count % 2) == 1 ? ProblemStatus.PointIsInside : ProblemStatus.PointIsOutside;
            return result2;
        }

        public static bool IsSimplePolygon(Polygon polygon)
        {
            var count = polygon.Points.Count;
            for (var i = 0; i < count - 2; i++)
            {
                for (var j = i + 2; j < count - 1; j++)
                {
                    if (DoIntersect(polygon.Points[i],
                        polygon.Points[(i + 1) % count],
                        polygon.Points[j],
                        polygon.Points[(j + 1) % count]))
                    {
                        return false;
                    }
                }
                if (i != 0)
                {
                    if (DoIntersect(polygon.Points[i],
                        polygon.Points[(i + 1) % count],
                        polygon.Points[count - 1],
                        polygon.Points[0]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private static Point GetPointOutsidePolygon(Polygon polygon)
        {
            return new Point(polygon.Points.
                                 Max(p => p.X) + 1, polygon.PointToCheck.Y);
        }

        public static bool DoIntersect(Point p1, Point q1, Point p2, Point q2)
        {
            // Find the four orientations needed for general and
            // special cases
            int o1 = SegmentsOrientation(p1, q1, p2);
            int o2 = SegmentsOrientation(p1, q1, q2);
            int o3 = SegmentsOrientation(p2, q2, p1);
            int o4 = SegmentsOrientation(p2, q2, q1);

            // General case
            if (o1 != o2 && o3 != o4)
                return true;

            // Special Cases
            // p1, q1 and p2 are colinear and p2 lies on segment p1q1
            if (o1 == 0 && IsOnSegment(p1, p2, q1)) return true;

            // p1, q1 and p2 are colinear and q2 lies on segment p1q1
            if (o2 == 0 && IsOnSegment(p1, q2, q1)) return true;

            // p2, q2 and p1 are colinear and p1 lies on segment p2q2
            if (o3 == 0 && IsOnSegment(p2, p1, q2)) return true;

            // p2, q2 and q1 are colinear and q1 lies on segment p2q2
            if (o4 == 0 && IsOnSegment(p2, q1, q2)) return true;

            return false; // Doesn't fall in any of the above cases
        }

        private static bool IsOnSegment(Point p, Point q, Point r)
        {
            return q.X <= Math.Max(p.X, r.X) && q.X >= Math.Min(p.X, r.X) &&
                   q.Y <= Math.Max(p.Y, r.Y) && q.Y >= Math.Min(p.Y, r.Y);
        }

        private static int SegmentsOrientation(Point p, Point q, Point r)
        {
            double val = (q.Y - p.Y) * (r.X - q.X) -
                         (q.X - p.X) * (r.Y - q.Y);

            if (Math.Abs(val) < Tolerance) return 0; // colinear
            return (val > 0) ? 1 : 2; // clock or counterclock wise
        }

        private const double Tolerance = 1e-10;
    }
}