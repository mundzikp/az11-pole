﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AZ11Pole.Models
{
    public class ProblemStatus
    {
        private ProblemStatus(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        public static ProblemStatus WronglyDefined
        {
            get { return new ProblemStatus("Wrongly defined polygon"); }
        }

        public static ProblemStatus NotSimple
        {
            get { return new ProblemStatus("Polygon is not simple"); }
        }

        public static ProblemStatus PointIsInside
        {
            get { return new ProblemStatus("Point is inside polygon"); }
        }

        public static ProblemStatus PointIsOutside
        {
            get { return new ProblemStatus("Point is outside polygon"); }
        }
    }
}